import requests
import sys
import pprint
import texttable as tt

session = requests.session()

print(sys.argv[1])
url = "https://acg-review.services.alkira2.net/api/v1/acgs?name=" + sys.argv[1]
r = session.get(url)
print(r)
print(r.json())

id = r.json()[0]["id"]
print("Id = " + id)

r = session.get("https://acg-review.services.alkira2.net/api/v1/acgs/" + id)

tab = tt.Texttable()

tab.header(["Service Name", "Branch", "Status", "Tracking"])

services = r.json()["services"]

all_ready = True



for service in services:
    tab.add_row([service["name"], service["branch"], service["status"], service["track"]])

    if service["status"] != "deployed":
        all_ready = False

print(tab.draw())

print("All ready? : " + str(all_ready))

