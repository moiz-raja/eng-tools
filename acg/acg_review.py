import requests
import texttable as tt

session = requests.session()

acgs = session.get("https://acg-review.services.alkira2.net/api/v1/acgs?limit=100")

acgs = acgs.json()

print("Currently running acgs : " + str(len(acgs)))

acgs.sort(key=lambda acg: acg["updatedAt"], reverse=True)

tab = tt.Texttable()

tab.header(["Name", "Id", "UpdatedAt"])
tab.set_cols_width([50, 30, 30])
for acg in acgs:
    tab.add_row([acg["name"], acg["id"], acg["updatedAt"]])
#    print(acg["name"])

print(tab.draw())
