import requests
import sys

services = []

for i in range(1, len(sys.argv)):
    arg = sys.argv[i].split(":")
    services.append({
        "name" : arg[0],
        "branch" : arg[1],
        "track" : True,
        "skip" : False
    })


review = {}


review["name"] = "moiz-sanity"
review["services"] = services

print(review)

session = requests.session()

r = session.post("https://acg-review.services.alkira2.net/api/v1/acgs", json=review)

print(r.json())

