import requests
import sys

session = requests.session()
url = "https://acg-review.services.alkira2.net/api/v1/acgs?name=" + sys.argv[1]
print(url)
r = session.get(url)

id = r.json()[0]["id"]
print("Id = " + id)

url = "https://acg-review.services.alkira2.net/api/v1/acgs/" + id
print(url)
r = session.get(url)

orig_services = r.json()["services"]

services = []

for i in range(2, len(sys.argv)):
    arg = sys.argv[i].split(":")
    for orig_service in  orig_services:
        if orig_service["name"] == arg[0]:
            service_id = orig_service["id"]
            print(service_id)
            r = session.put("https://acg-review.services.alkira2.net/api/v1/acgs/"+ id + "/services/" + service_id, json={
                                                                                                                        "skip" : False,
                                                                                                                        "name" : arg[0],
                                                                                                                        "branch" : arg[1],
                                                                                                                        "track" : True
                                                                                                                    })
            print(r.json())
