import requests
import random

HEADERS = {
    "x-tenant-id" : "4"
}

PRIORITIES = ["MEDIUM", "HIGH", "LOW", "CRITICAL"]
STATUSES = ["ACTIVE", "RESOLVED"]
TYPES = ["NETWORK_STATUS","NETWORK_PROVISIONING","OPERATIONS","MISCELLANEOUS"]


for i in range(1, 100):
    res = requests.post("http://notification:3000/internal/alerts", json={
      "description": "Test" + str(i),
      "id": "string" + str(i),
      "markedRead": False,
      "priority": PRIORITIES[random.randint(0, 3)],
      "status": STATUSES[random.randint(0, 1)],
      "summary": "summary",
      "tags": [
        {
          "name": "string",
          "value": "string"
        }
      ],
      "type": TYPES[random.randint(0, 1)]
    }, headers= HEADERS)

    print(str(res.status_code) + " : " + str(i))
