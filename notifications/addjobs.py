import requests
import random
import uuid

HEADERS = {
    "x-tenant-id" : "1"
}

STATUSES = ["PENDING","IN_PROGRESS","SUCCESS","FAILED"]
TYPES = ["NETWORK_PROVISIONING"]
TASK_TYPES = ["CONNECTOR_PROVISIONING","SERVICE_PROVISIONING"]


for i in range(1, 10):
    res = requests.post("http://localhost:6500/internal/jobs", json={
      "description": "Test" + str(i),
      "id": "string" + str(i),
      "status": STATUSES[random.randint(0, 3)],
      "summary": "job summary " + str(i),
      "tasks" : [
          {
            "id" : str(uuid.uuid4()),
            "type" : TASK_TYPES[random.randint(0, 1)],
            "summary" : "task summary 1",
            "description": "task description 1",
            "progress" : random.randint(0,100)
          },
          {
              "id": str(uuid.uuid4()),
              "type": TASK_TYPES[random.randint(0, 1)],
              "summary": "task summary 2",
              "description": "task description 2",
              "progress": random.randint(0, 100)
          },
          {
              "id": str(uuid.uuid4()),
              "type": TASK_TYPES[random.randint(0, 1)],
              "summary": "task summary 3",
              "description": "task description 3",
              "progress": random.randint(0, 100)
          },

          {
              "id": str(uuid.uuid4()),
              "type": TASK_TYPES[random.randint(0, 1)],
              "summary": "task summary 3",
              "description": "task description 3",
              "progress": random.randint(0, 100)
          },
          {
              "id": str(uuid.uuid4()),
              "type": TASK_TYPES[random.randint(0, 1)],
              "summary": "task summary 3",
              "description": "task description 3",
              "progress": random.randint(0, 100)
          },
          {
              "id": str(uuid.uuid4()),
              "type": TASK_TYPES[random.randint(0, 1)],
              "summary": "task summary 3",
              "description": "task description 3",
              "progress": random.randint(0, 100)
          },
          {
              "id": str(uuid.uuid4()),
              "type": TASK_TYPES[random.randint(0, 1)],
              "summary": "task summary 3",
              "description": "task description 3",
              "progress": random.randint(0, 100)
          },
          {
              "id": str(uuid.uuid4()),
              "type": TASK_TYPES[random.randint(0, 1)],
              "summary": "task summary 3",
              "description": "task description 3",
              "progress": random.randint(0, 100)
          },
          {
              "id": str(uuid.uuid4()),
              "type": TASK_TYPES[random.randint(0, 1)],
              "summary": "task summary 3",
              "description": "task description 3",
              "progress": random.randint(0, 100)
          },
          {
              "id": str(uuid.uuid4()),
              "type": TASK_TYPES[random.randint(0, 1)],
              "summary": "task summary 3",
              "description": "task description 3",
              "progress": random.randint(0, 100)
          },
          {
              "id": str(uuid.uuid4()),
              "type": TASK_TYPES[random.randint(0, 1)],
              "summary": "task summary 3",
              "description": "task description 3",
              "progress": random.randint(0, 100)
          },
          {
              "id": str(uuid.uuid4()),
              "type": TASK_TYPES[random.randint(0, 1)],
              "summary": "task summary 3",
              "description": "task description 3",
              "progress": random.randint(0, 100)
          },
          {
              "id": str(uuid.uuid4()),
              "type": TASK_TYPES[random.randint(0, 1)],
              "summary": "task summary 3",
              "description": "task description 3",
              "progress": random.randint(0, 100)
          },
          {
              "id": str(uuid.uuid4()),
              "type": TASK_TYPES[random.randint(0, 1)],
              "summary": "task summary 3",
              "description": "task description 3",
              "progress": random.randint(0, 100)
          },
          {
              "id": str(uuid.uuid4()),
              "type": TASK_TYPES[random.randint(0, 1)],
              "summary": "task summary 3",
              "description": "task description 3",
              "progress": random.randint(0, 100)
          },
          {
              "id": str(uuid.uuid4()),
              "type": TASK_TYPES[random.randint(0, 1)],
              "summary": "task summary 3",
              "description": "task description 3",
              "progress": random.randint(0, 100)
          },
          {
              "id": str(uuid.uuid4()),
              "type": TASK_TYPES[random.randint(0, 1)],
              "summary": "task summary 3",
              "description": "task description 3",
              "progress": random.randint(0, 100)
          },
      ],
      "tags": [
        {
          "name": "string",
          "value": "string"
        }
      ],
      "type": TYPES[random.randint(0, 0)]
    }, headers= HEADERS)

    print(str(res.status_code) + " : " + str(i))
