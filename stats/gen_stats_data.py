import requests
import sys

session = requests.session()

tenant_id = sys.argv[1]
tenant_network_id = sys.argv[2]
max_time_before_in_millis = 10000000

print("Generating stats for tenant: " + str(tenant_id) + ", tenantNetwork: " + str(tenant_network_id))

tenant_network = session.get("http://tenant-provisioning-service:3000/tenantnetworks/" + str(tenant_network_id), headers = {"x-tenant-id" : str(tenant_id)})

tenant_network_json = tenant_network.json()

segments = tenant_network_json["segments"]

segment_names = []
for segment in segments:
    segment_names.append(segment["internalName"])

print(segment_names)

indexNames = ["flow-stats-applications-5m",
              "flow-stats-applications-60m",
              "flow-stats-rules-5m",
              "flow-stats-rules-60m",
              "flow-stats-talkers-5m",
              "flow-stats-talkers-60m"]

for indexName in indexNames:
    print("Running data generation for index : " + indexName)
    session.post("http://stats:3000/stats/generate?tenantIds=" + str(tenant_id) + "&maxTimeBeforeInMillis=" + str(max_time_before_in_millis) + "&segmentNames="+segment_names[0]+"&indexName=" + indexName)
    print("Done generating data for index : " + indexName)


